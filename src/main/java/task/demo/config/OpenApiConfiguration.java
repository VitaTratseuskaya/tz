package task.demo.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.aspectj.bridge.Version.getText;

@Configuration
public class OpenApiConfiguration {

    @Bean
    public OpenAPI demoApi() {
        return new OpenAPI()
                .info(new Info().title("Demo API")
                        .description("<br> Another info: " + getText())
                        .version("v0.0.1")
                        .summary(getText()));

    }

    public ExternalDocumentation getExternalDocs() {
        ExternalDocumentation externalDocumentation = new ExternalDocumentation();
        externalDocumentation.setDescription("test");
        return externalDocumentation;
    }

  /*  public String getText() {
        try {
            InputStream is = getClass().getClassLoader().getResourceAsStream("descriptionSwagger.txt");
            return IOUtils.toString(is, StandardCharsets.UTF_8);
        } catch (Exception e) {
            return "";
        }
    }*/
}

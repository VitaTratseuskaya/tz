package task.demo.advice.exception;

public class CreateException extends RuntimeException {

    public CreateException(String message) {
        super(message);
    }

}

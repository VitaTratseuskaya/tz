package task.demo.user.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Schema
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "users")
public class UserEntity {


    @JsonProperty(value = "id")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JsonProperty(value = "fullName")
    @Column(name = "fullname")
    private String fullName;

    @JsonProperty(value = "email")
    @Column(name = "email")
    private String email;

    @JsonProperty(value = "password")
    @Column(name = "password")
    private String password;

    @JsonProperty(value = "photo")
    @Column(name = "photo")
    private String photo;

    @JsonProperty(value = "status")
    @Column(name = "status")
    private String status;
}

package task.demo.user.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import task.demo.util.DefaultResponse;
import task.demo.advice.exception.NotFoundException;
import task.demo.user.dto.UserStatusRequestDto;
import task.demo.user.dto.response.UserStatusResponseDto;
import task.demo.user.dto.UserWithoutPassword;
import task.demo.user.dto.request.UserCreateRequestDto;
import task.demo.user.dto.response.UserCreateResponseDto;
import task.demo.user.service.UserService;

@Tag(name = "User controller", description = "Api for working with users")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
public class UserController {

    private final UserService service;

    @Operation(summary = "Create user")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {
                    @Content(schema = @Schema(implementation = UserCreateResponseDto.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = {
                    @Content(schema = @Schema(implementation = DefaultResponse.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "409", content = {
                    @Content(schema = @Schema(implementation = DefaultResponse.class), mediaType = "application/json") }) })
    @PostMapping("/")
    public ResponseEntity<UserCreateResponseDto> createUser(@RequestBody UserCreateRequestDto requestDto) {
        return new ResponseEntity<>(service.createUser(requestDto), HttpStatus.OK);
    }

    @Operation(summary = "Find user by id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {
                    @Content(schema = @Schema(implementation = UserWithoutPassword.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = {
                    @Content(schema = @Schema(implementation = NotFoundException.class), mediaType = "application/json") })})
    @GetMapping("/{id}")
    public ResponseEntity<UserWithoutPassword> findById(@PathVariable Long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @Operation(summary = "Change status")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {
                    @Content(schema = @Schema(implementation = UserStatusResponseDto.class), mediaType = "application/json") }),
            @ApiResponse(responseCode = "404", content = {
                    @Content(schema = @Schema(implementation = DefaultResponse.class), mediaType = "application/json") })})
    @GetMapping("/changeStatus")
    public ResponseEntity<UserStatusResponseDto> changeStatus(@RequestBody UserStatusRequestDto requestDto) {
        return new ResponseEntity<>(service.changeStatus(requestDto), HttpStatus.OK);
    }
}

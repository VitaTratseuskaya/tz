package task.demo.user.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import task.demo.advice.exception.CreateException;
import task.demo.advice.exception.NotFoundException;
import task.demo.user.dto.UserStatusRequestDto;
import task.demo.user.dto.response.UserStatusResponseDto;
import task.demo.user.dto.UserWithoutPassword;
import task.demo.user.dto.request.UserCreateRequestDto;
import task.demo.user.dto.response.UserCreateResponseDto;
import task.demo.user.entity.UserEntity;
import task.demo.user.repository.UserRepository;
import task.demo.user.service.UserService;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;


    @Override
    public UserCreateResponseDto createUser(UserCreateRequestDto requestDto) {

        Optional<UserEntity> optionalUserByEmail = userRepository.findByEmail(requestDto.getEmail());

        if(optionalUserByEmail.isPresent()) {
            throw new CreateException("User with email: " + requestDto.getEmail() + " already exists");
        }

        return create(requestDto);
     }

    @Override
    public UserWithoutPassword findById(Long id) {
        Optional<UserEntity> optionalUserById = userRepository.findById(id);

        if (optionalUserById.isEmpty()) {
            throw new NotFoundException("User with id: " + id + " not found");
        }

        UserEntity user = optionalUserById.get();
        UserWithoutPassword result = new UserWithoutPassword();
        result.setEmail(user.getEmail());
        result.setFullName(user.getFullName());
        result.setPhoto(user.getPhoto());
        result.setStatus(user.getStatus());

        return result;
    }

    @Override
    public UserStatusResponseDto changeStatus(UserStatusRequestDto requestDto) {
        Optional<UserEntity> optionalUserById = userRepository.findById(requestDto.getId());

        if (optionalUserById.isEmpty()) {
            throw new NotFoundException("User with id: " + requestDto.getId() + " not found");
        }

        UserEntity user = optionalUserById.get();
        user.setStatus(requestDto.getNewStatus());
        userRepository.save(user);

        UserStatusResponseDto result = new UserStatusResponseDto();
        result.setId(user.getId());
        result.setOldStatus(user.getStatus());
        result.setNewStatus(requestDto.getNewStatus());

        return result;
    }


    private UserCreateResponseDto create(UserCreateRequestDto requestDto) {

        UserEntity user = new UserEntity();
        user.setEmail(requestDto.getEmail());
        user.setFullName(requestDto.getFullname());
        user.setPhoto(requestDto.getPhoto());
        user.setStatus(requestDto.getStatus());
        user.setPassword(requestDto.getPassword());

        userRepository.save(user);

        UserCreateResponseDto result = new UserCreateResponseDto();
        result.setId(user.getId());

        return result;
    }
}

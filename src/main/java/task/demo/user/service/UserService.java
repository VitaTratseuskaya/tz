package task.demo.user.service;

import task.demo.user.dto.UserStatusRequestDto;
import task.demo.user.dto.response.UserStatusResponseDto;
import task.demo.user.dto.UserWithoutPassword;
import task.demo.user.dto.response.UserCreateResponseDto;
import task.demo.user.dto.request.UserCreateRequestDto;

public interface UserService {

    UserCreateResponseDto createUser(UserCreateRequestDto requestDto);

    UserWithoutPassword findById(Long id);

    UserStatusResponseDto changeStatus(UserStatusRequestDto requestDto);
}

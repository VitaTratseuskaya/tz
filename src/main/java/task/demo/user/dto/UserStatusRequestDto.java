package task.demo.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserStatusRequestDto {

    @JsonProperty(value = "id")
    private Long id;
    @JsonProperty(value = "newStatus")
    private String newStatus;

}

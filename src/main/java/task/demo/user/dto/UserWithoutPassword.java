package task.demo.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserWithoutPassword {

    @JsonProperty(value = "fullName")
    private String fullName;
    @JsonProperty(value = "email")
    private String email;
    @JsonProperty(value = "photo")
    private String photo;
    @JsonProperty(value = "status")
    private String status;
}

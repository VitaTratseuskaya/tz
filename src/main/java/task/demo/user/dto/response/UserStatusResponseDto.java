package task.demo.user.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserStatusResponseDto {

    @JsonProperty(value = "id")
    private Long id;
    @JsonProperty(value = "oldStatus")
    private String oldStatus;
    @JsonProperty(value = "newStatus")
    private String newStatus;
}
